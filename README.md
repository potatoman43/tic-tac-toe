Here is my tic tac toe game.

It meets the following requirements:

* Allows for a human player
* Play against a computer player
* Has a UI


However, it does NOT meet the following requirements, due to lack of time:

* The computer player should never lose and should win whenever possible.

It also does not include automated tests due to lack of time. But I think it was written in a way that would make
unit testing pretty easy.

### Steps to run the web app:

1. npm install
2. npm run start
3. Open up your favorite browser and go to `localhost:3000`


This app was bootstrapped using TypeScript-React-Starter (which basically is a wrapper around `create-react-app`).

Most of the logic is contained within the following files:

* src/lib/Game/index.ts
* src/lib/Player/index.ts
* src/components/TicTacToeBoard.tsx
* src/components/TicTacToeBoardContainer.tsx

This follows the MVC paradigm for the most part

Models:
* src/lib/Game/index.ts
* src/lib/Player/index.ts

Views:
* src/components/TicTacToeBoard.tsx

Controller:
* src/components/TicTacToeBoardContainer.tsx


I included a few `fixme` comments to recognize areas that are lacking and are not well thought out.
Mainly, I would have created a cleaner way to serialize the model so that it could better fit in react's ecosystem.

Then I would probably make the `Game` class extend `EventEmitter` so that we could trigger updates to the react app state
in a more intuitive manner.







import * as React from "react";
import * as ReactDOM from "react-dom";
import { TicTacToeBoardContainer } from "./components/TicTacToeBoardContainer";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(
  <TicTacToeBoardContainer />,
  document.getElementById("root") as HTMLElement,
);
registerServiceWorker();

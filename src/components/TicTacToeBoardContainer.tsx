import * as React from "react";
import {
  TicTacToeBoard,
  TicTacToeBoardProps
} from './TicTacToeBoard';

import {
  TicTacToeGame,
  TicTacToeGameOptions,
  TicTacToeGameStatus,
} from "../lib/Game";

import {
  Player,
  PlayerOptions,
  PlayerSymbol,
  PlayerType,
} from "../lib/Player";

export interface TicTacToeBoardContainerState {
  activePlayer: string;
  board: PlayerSymbol[][];
  gameStatus: TicTacToeGameStatus;
  numGamesPlayed: number;
  player1Id: string;
  player2Id: string;
  player1Type: PlayerType;
  player2Type: PlayerType;
  statusMessage: string;
}

export class TicTacToeBoardContainer extends React.Component <{}, TicTacToeBoardContainerState> {
  private game: TicTacToeGame;
  constructor(props: any) {
    super(props);

    this.game = this.createNewGame();
    this.state = this.updateAppState(0);
  }

  public componentDidUpdate(prevProps: {}, prevState: TicTacToeBoardContainerState) {
    // If the user updates the player Type to 'Computer' for the active player,
    // we should trigger the computer player to pick the next position.
    if (prevState.activePlayer === 'player 1' && this.state.activePlayer === 'player 1') {
      const player1WasAPerson = prevState.player1Type === PlayerType.Person;
      const player1IsNowAComputer = this.state.player1Type === PlayerType.Computer;
      if (player1WasAPerson && player1IsNowAComputer) {
        this.game.computerPickPosition();
        const numGamesPlayed = this.state.numGamesPlayed;
        const appState = this.updateAppState(numGamesPlayed);
        this.setState(appState);
      }
    }

    if (prevState.activePlayer === 'player 2' && this.state.activePlayer === 'player 2') {
      const player2WasAPerson = prevState.player2Type === PlayerType.Person;
      const player2IsNowAComputer = this.state.player2Type === PlayerType.Computer;

      if (player2WasAPerson && player2IsNowAComputer) {
        this.game.computerPickPosition();
        const numGamesPlayed = this.state.numGamesPlayed;
        const appState = this.updateAppState(numGamesPlayed);
        this.setState(appState);
      }
    }

  }

  public handleNewGameButtonClick = (): void => {
    const numGamesPlayed = this.state.numGamesPlayed + 1;
    this.game = this.createNewGame();
    console.log(this.game);
    const newAppState = this.updateAppState(numGamesPlayed);
    this.setState(newAppState);
  }

  public handlePlayPosition = (i: number, j: number): void => {
    if (this.state.gameStatus === TicTacToeGameStatus.Finished) { return; }
    this.game.playPosition(i, j);
    const numGamesPlayed = this.state.numGamesPlayed;
    this.setState(this.updateAppState(numGamesPlayed));
  }

  public updateAppState = (numGamesPlayed: number) => {
   // fixme (mk): clean up serialization...
    const appState: TicTacToeBoardContainerState = {
      activePlayer: this.game.getActivePlayerId(),
      board: this.game.getBoard(),
      gameStatus: this.game.getGameStatus(),
      numGamesPlayed,
      player1Id: this.game.getPlayer1().getId(),
      player2Id: this.game.getPlayer2().getId(),
      player1Type: this.game.getPlayer1().getType(),
      player2Type: this.game.getPlayer2().getType(),
      statusMessage: this.getStatusMessage(),
    };
    return appState;
  }

  public createNewGame = () => {
    const player1 = this.createPlayer1();
    const player2 = this.createPlayer2();

    const opts: TicTacToeGameOptions = {
      boardSize: 3,
      player1,
      player2,
    };

    return new TicTacToeGame(opts);
  }

  public createPlayer1 = (): Player => {
    let type: PlayerType = PlayerType.Person;
    if (this.state) { type = this.state.player1Type; }
    const opts: PlayerOptions = {
      id: "player 1",
      symbol: PlayerSymbol.X,
      type
    };
    return new Player(opts);
  }

  public createPlayer2 = (): Player => {
    let type: PlayerType = PlayerType.Person;
    if (this.state) { type = this.state.player2Type; }
    const opts: PlayerOptions = {
      id: "player 2",
      symbol: PlayerSymbol.O,
      type
    };
    return new Player(opts);
  }

  public getStatusMessage = (): string => {
    if (this.game.getGameStatus() === TicTacToeGameStatus.Finished) {
      if (this.game.getWinnerPlayerId()) {
        return `Congrats ${this.game.getActivePlayerId()}, you won the game!`;
      } else {
        return `Tie Game!`;
      }
    } else {
      return `${this.game.getActivePlayerId()}'s turn...`;
    }
  }

  public handlePlayerTypeChange = (playerId: string, type: PlayerType): void => {
    const player = this.game.getPlayerFromId(playerId);
    player.setType(type);
    const numGamesPlayed = this.state.numGamesPlayed;
    const appState = this.updateAppState(numGamesPlayed);
    this.setState(appState);
  }

  public render() {
    const props: TicTacToeBoardProps = {
      ...this.state,
      handleNewGameClick: this.handleNewGameButtonClick.bind(this),
      handlePlayPosition: this.handlePlayPosition.bind(this),
      handlePlayerTypeChange: this.handlePlayerTypeChange.bind(this)
    };
    return (
      <TicTacToeBoard
        {...props}
      />
    );
  }
}


import * as React from 'react';

import {
    PlayerSymbol,
    PlayerType
} from '../lib/Player';

import {
    TicTacToeGameStatus
} from '../lib/Game';

import {
    TicTacToeBoardContainerState
} from './TicTacToeBoardContainer';


export interface TicTacToeBoardProps extends TicTacToeBoardContainerState {
    handleNewGameClick: () => {};
    handlePlayPosition: (i: number, j: number) => {};
    handlePlayerTypeChange: (playerId: string, type: PlayerType) => {};
}

export const TicTacToeBoard = (props: TicTacToeBoardProps): JSX.Element => {
    return (
        <div className='tic-tac-toe'>
            { renderStatusMessage(props) }
            { renderPlayerInformation(props.player1Id, props.player1Type, 'X', props)}
            { renderPlayerInformation(props.player2Id, props.player2Type, 'O', props)}
            { renderBoard(props) }
        </div>
    );
}

const renderStatusMessage = (props: TicTacToeBoardProps) => {
    return (
        <div className='status-message'>
            { props.statusMessage }
        </div>
    );
}

const renderPlayerInformation = (playerId: string, playerType: PlayerType, symbol: string, props: TicTacToeBoardProps) => {
    const isPerson = playerType === PlayerType.Person;
    return (
    <div className='player-info'>
        <div> { `${playerId}: ${symbol} `} </div>
        <label>
            <input type="radio" value={PlayerType.Person} checked={isPerson}
                onChange={() => {props.handlePlayerTypeChange(playerId, PlayerType.Person); }}/>
            Person
        </label>
        <label>
            <input type="radio" value={PlayerType.Computer} checked={!isPerson}
                onChange={() => {props.handlePlayerTypeChange(playerId, PlayerType.Computer); }}/>
            AI
        </label>
    </div>
    );
}

const renderNewGameButton = (props: TicTacToeBoardProps) => {
    if (props.gameStatus !== TicTacToeGameStatus.Finished) {
     return null;
    }

    return (
        <div className='row'>
            <div className='new-game button'
            onClick={props.handleNewGameClick}>
                New Game
            </div>
        </div>
    );
}


const renderBoard = (props: TicTacToeBoardProps) => {
    const boardSize = props.board.length;
    const rows = [];
    for (let i = 0; i < boardSize; i++) {
        rows.push(renderRow(props, i));
    }
    return (
        <div className='tic-tac-toe-board'>
            { rows }
            { [renderNewGameButton(props)]}
        </div>
    )
}

const renderRow = (props: TicTacToeBoardProps, i: number) => {
    const row = props.board[i];
    const buttons = row.map((symbol: PlayerSymbol, j: number) => {
        let displaySymbol: string;
        switch (symbol) {
            case PlayerSymbol.X: {
                displaySymbol = "X";
                break;
            }
            case PlayerSymbol.O: {
                displaySymbol= "O";
                break;
            }
            default: {
                displaySymbol = " ";
            }
        }
        return (
            <span className='button'
                onClick={() => { props.handlePlayPosition(i,j)}}
                key={`${i}${j}`}
            >
                { displaySymbol }
            </span>

        );
    });
    return (
        <div className='row'
            key={`${i}`}>
            { buttons }
            <br/>
        </div>
    );
}
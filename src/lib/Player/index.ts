
export enum PlayerType {
    Computer,
    Person,
}

export enum PlayerSymbol {
    X,
    O,
    Empty,
}

export interface PlayerOptions {
    id: string;
    type: PlayerType;
    symbol: PlayerSymbol;
}

export class Player {
    private id: string;
    private type: PlayerType;
    private symbol: PlayerSymbol;
    private wins: number;

    constructor(opts: PlayerOptions) {
        if (opts.id === "") {
            throw new Error("A player must have a non empty id");
        }

        this.id = opts.id;
        this.type = opts.type;
        this.symbol = opts.symbol;
        this.wins = 0;
    }

    public getId = (): string => {
        return this.id;
    }

    public setType = (t: PlayerType): void => {
        this.type = t;
    }

    public getType = (): PlayerType => {
        return this.type;
    }

    public setSymbol = (symbol: PlayerSymbol): void => {
        this.symbol = symbol;
    }

    public getSymbol = (): PlayerSymbol  => {
        return this.symbol;
    }

    public playerWonHook = (): void => {
        const wins = this.getWins();
        this.setWins(wins + 1);
    }

    public setWins = (wins: number): void => {
        this.wins = wins;
    }

    public getWins = (): number => {
        return this.wins;
    }
}

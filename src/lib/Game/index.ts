import {
    Player,
    PlayerSymbol,
    PlayerType
} from "../Player";

export enum TicTacToeGameStatus {
    Active,
    Finished,
}

export interface TicTacToeGameOptions {
    boardSize: number;
    player1: Player;
    player2: Player;
}

const isThereAWinner = (arr: PlayerSymbol[]): boolean => {
    if (arr[0] === PlayerSymbol.Empty) { return false; }
    return arr.every((value: PlayerSymbol, i: number, copy: PlayerSymbol[]): boolean =>  {
        return value === copy[0];
    });
};

interface Position {
    i: number,
    j: number
};


// fixme (mk): This should most likely extend event emitter in order to trigger
// react app state updates. In the meantime, I will rely on carefully placed
// hooks.
export class TicTacToeGame {
    private board: PlayerSymbol[][];
    private status: TicTacToeGameStatus;
    private player1: Player;
    private player2: Player;
    private activePlayerId: string;

    constructor(opts: TicTacToeGameOptions) {
        if (opts.boardSize < 3) {
            throw new Error("Tic tac toe board must be of size 3 or greater");
        }

        this.board = new Array(opts.boardSize);
        for (let i = 0; i < opts.boardSize; i++) {
            this.board[i] = new Array(opts.boardSize).fill(PlayerSymbol.Empty);
        }

        this.status = TicTacToeGameStatus.Active;
        this.player1 = opts.player1;
        this.player2 = opts.player2;
        this.activePlayerId = this.player1.getId();

        if (this.player1.getType() === PlayerType.Computer) {
            this.computerPickPosition();
        }
    }

    public getGameStatus = (): TicTacToeGameStatus => {
        return this.status;
    }

    public getActivePlayerId = (): string => {
        return this.activePlayerId;
    }

    public getPlayer1 = (): Player => {
        return this.player1;
    }

    public getPlayer2 = (): Player => {
        return this.player2;
    }

    public getBoard = (): any[] =>  {
        return this.board;
    }

    public playPosition = (i: number, j: number): any[] => {
        if (this.status === TicTacToeGameStatus.Finished) { return this.board; }
        console.log('play position');
        if (this.board[i][j] === PlayerSymbol.Empty) {
            const activePlayer = this.getPlayerFromId(this.getActivePlayerId());
            this.board[i][j] = activePlayer.getSymbol();
            if (this.getWinnerPlayerId()) {
                this.status = TicTacToeGameStatus.Finished;
                return this.board;
            } else if (this.boardIsFull()) {
                this.status = TicTacToeGameStatus.Finished;
                return this.board;
            }
            this.switchActivePlayer();
        }
        return this.board;
    }

    /**
     * getWinnerPlayerId returns the player id of the winner, if there is one.
     * Otherwise, it returns an empty string to indicate that nobody has won
     * yet.
     */
    public getWinnerPlayerId = (): string => {
        if (this.getWinnerFromColsAndRows()) {
            return this.getWinnerFromColsAndRows();
        } else if (this.getWinnerFromDiagonals()) {
            return this.getWinnerFromDiagonals();
        } else {
            return "";
        }
    }

    public boardIsFull = (): boolean => {
        for(let i = 0; i < this.board.length; i++) {
            if (this.board[i].includes(PlayerSymbol.Empty)) {
                return false;
            }
        }
        return true;
    }

    public getPlayerFromId = (id: string): Player => {
        if (this.player1.getId() === id) {
            return this.player1;
        } else if (this.player2.getId() === id) {
            return this.player2;
        } else {
            throw new Error("Could not get player from id");
        }
    }

    public computerPickPosition = (): void => {
        const positions = this.getAvailablePositions();
        const position = positions[Math.floor(Math.random() * positions.length)];
        this.playPosition(position.i, position.j);
    }

    private switchActivePlayer = (): void => {
        if (this.activePlayerId === this.player1.getId()) {
            this.activePlayerId = this.player2.getId();
        } else if (this.activePlayerId === this.player2.getId()) {
            this.activePlayerId = this.player1.getId();
        }

        const player = this.getPlayerFromId(this.activePlayerId);
        if (player.getType() === PlayerType.Computer) {
            this.computerPickPosition();
        }
    }

    private getAvailablePositions = (): Position[] => {
        // fixme (mk): Could avoid n^2 operation by maintaining
        // a list of empty positions.
        if (this.boardIsFull()) { return []; }
        const positions: Position[] = [];
        for(let i = 0; i < this.board.length; i++) {
            for (let j = 0; j < this.board.length; j++) {
                if (this.board[i][j] === PlayerSymbol.Empty) {
                    positions.push({i,j});
                }
            }
        }
        return positions;
    }

    private getWinnerFromColsAndRows = (): string => {
        for (let i = 0; i < this.board.length; i++) {
            const row = [], col = [];

            // fixme (mk): there is definitely a cleaner way to do this...
            for (let j = 0; j < this.board.length; j++) {
                row.push(this.board[i][j]);
                col.push(this.board[j][i]);
            }

            if (isThereAWinner(row)) {
                return this.getPlayerIdFromSymbol(row[0]);
            }
            if (isThereAWinner(col)) {
                return this.getPlayerIdFromSymbol(col[0]);
            }
        }
        return "";
    }

    private getWinnerFromDiagonals = (): string => {
        const positiveDiag = [], negtativeDiag = [];

        for (let i = 0; i < this.board.length; i++) {
            negtativeDiag.push(this.board[i][i]);
            positiveDiag.push(this.board[i][this.board.length - i - 1]);
        }

        if (isThereAWinner(positiveDiag)) {
            return this.getPlayerIdFromSymbol(positiveDiag[0]);
        }
        if (isThereAWinner(negtativeDiag)) {
            return this.getPlayerIdFromSymbol(negtativeDiag[0]);
        }
        return "";
    }

    private getPlayerIdFromSymbol = (symbol: PlayerSymbol): string => {
        if (this.player1.getSymbol() === symbol) {
            return this.player1.getId();
        } else if (this.player2.getSymbol() === symbol) {
            return this.player2.getId();
        } else {
            throw new Error("Could not get player from symbol");
        }
    }
}

